{
  const {
    html,
  } = Polymer;
  /**
    `<image-container>` Description.

    Example:

    ```html
    <image-container></image-container>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --image-container | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class ImageContainer extends Polymer.Element {

    static get is() {
      return 'image-container';
    }

    static get properties() {
      return {
        image: {
        type: Object,
        value: () => ({})
        }
      };
    }

  }

  customElements.define(ImageContainer.is, ImageContainer);
}
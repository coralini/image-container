# &lt;image-container&gt;

## Description
This component displays an image that should be declared within an Object along with the style specification.
You can use the regular styling or customize with the "custom" selector available in a Mixin.

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
Example:
```html
    <image-container
    image='{"src": "http://getwallpapers.com/wallpaper/full/c/3/1/193891.jpg",
    "styling": "regular"}'
    ></image-container>
```

## Styling
  The following custom properties and mixins are available for styling:

  ### Custom Properties
  | Custom Property     	| Selector | CSS Property | Value       |
  | -----------------------	| -------- | ------------ | ----------- |
  | --cells-fontDefault 	| :host    | font-family  |  sans-serif |
  |--image-container-display| card-body| display      | inline-flex |
  
  ### @apply
  | Mixins   		           | Selector | Value |
  | -------------------------  | -------- | ----- |
  | --image-container          | :host    | {}    |
  |--my-custom-container-theme | custom   | {}    |
